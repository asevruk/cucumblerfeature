package com.tasj;

import cucumber.api.java.After;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by asevruk on 4/15/2016.
 */
public class Hooks {
    @After("@clean")
    public  void clearData(){
        executeJavaScript("localStorage.clear()");
        open("http://todomvc.com");
    }
}
