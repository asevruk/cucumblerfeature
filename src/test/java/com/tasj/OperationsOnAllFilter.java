package com.tasj;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.*;
import static com.tasj.CustomsConditions.exactTexts;

/**
 * Created by asevruk on 4/14/2016.
 */
public class OperationsOnAllFilter {
    ElementsCollection tasks = $$("#todo-list>li");
    SelenideElement newTask=$("#new-todo");

    @When("^add tasks (.*)$")
    public void add_tasksa(List<String> taskText) {
        for (String task : taskText) {
            newTask.setValue(task).pressEnter();
        }
    }

    @Given("^open ToDoMVC page$")
    public void open_ToDoMVC_page() {
        open("https://todomvc4tasj.herokuapp.com/");
    }

    @Then("^tasks in the list (.*)$")
    public void tasks_in_the_list(List<String> taskText) {
        tasks.shouldHave(exactTexts(taskText));
    }

    @And("^items left counter is (.*)$")
    public void items_left_counter_is(int count) {
        $$("#todo-count").shouldHave(size(1));
    }

    @When("^edit (.*) to (.*)$")
    public void editTask(String oldText, String newText) {
        tasks.find(exactText(oldText)).doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newText).pressEnter();
    }

    @When("^delete (.*)$")
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();

    }

    @When("^toggle (.*) task$")
    public void toggle_a(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    @And("^press clear completed button$")
    public void press_clear_completed_button() {
        $("#clear-completed").click();
    }

    @When("^toggle all by button$")
    public void press_toggle_all() {
        $("#toggle-all").click();
    }

    @And("^press toggle all$")
    public void press_toggleAll() {
        $("#toggle-all").click();
    }
    @When("^start edit (.*) to (.*) and press outside$")
    public void start_edit_and_click_outside(String taskText, String newTaskText) {
        tasks.find(exactText(taskText)).doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
        newTask.click();
    }


    @When("^start edit (.*) to (.*) and press ESC$")
    public void start_edit_a_to_a_edited_and_press_ESC(String taskText, String newTaskText) {
        tasks.find(exactText(taskText)).doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText).pressEscape();
    }

    @When("^open active filter$")
    public void open_active_filter() {
        $(By.linkText("Active")).click();
    }

    @And("^open completed filter$")
    public void open_completed_filter() {
        $(By.linkText("Completed")).click();
    }

}
