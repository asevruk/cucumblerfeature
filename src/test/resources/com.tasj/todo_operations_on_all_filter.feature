Feature: ToDosSteps

  Background:
    Given open ToDoMVC page

  @clean
  Scenario: create task
    When add tasks a
    Then tasks in the list a
    And items left counter is 1

  @clean
  Scenario: edit task
    Given add tasks a
    When edit a to a-edited
    Then tasks in the list a-edited
    And items left counter is 1

  @clean
  Scenario: delete task
    Given add tasks a, b
    When delete a
    Then tasks in the list b
    And items left counter is 1

  @clean
  Scenario: complete task
    Given add tasks a, b
    When toggle a task
    Then tasks in the list a,b
    And items left counter is 1

  @clean
  Scenario: complete all task
    Given add tasks a, b
    When toggle all by button
    Then tasks in the list a, b
    And items left counter is 0

  @clean
  Scenario: clear completed task
    Given add tasks a, b
    When toggle a task
    And press clear completed button
    Then tasks in the list b
    And items left counter is 1

  @clean
  Scenario: reopen task
    Given add tasks a, b
    When toggle a task
    And toggle a task
    Then tasks in the list a, b
    And items left counter is 2

  @clean
  Scenario: reopen all task
    Given add tasks a, b
    When toggle a task
    And toggle b task
    And press toggle all
    Then tasks in the list a , b
    And items left counter is 2

  @clean
  Scenario: cancel edit  task
    Given add tasks a
    When start edit a to a-edited and press ESC
    Then tasks in the list a
    And items left counter is 1

  @clean
  Scenario: edit and click outside all task
    Given add tasks a
    When start edit a to a-edited and press outside
    Then tasks in the list a-edited
    And items left counter is 1

  @clean
  Scenario: move to active filter
    Given add tasks a
    When open active filter
    Then tasks in the list a
    And items left counter is 1

  @clean
  Scenario: move to completed filter
    Given add tasks a
    When toggle a task
    And open completed filter
    Then tasks in the list a
    And items left counter is 0


